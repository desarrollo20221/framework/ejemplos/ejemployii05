<?php
namespace app\widgets;
use yii\base\Widget;
use yii\helpers\Html;

class Botones extends Widget {
    public $opciones = [];

    public function init() {
    parent::init();
    }


    public function run() {
?>    
    <div class = "col-lg-4">
        <h2><?= $this->opciones["titulo"] ?></h2>

        <p><?= $this->opciones["contenido"] ?></p>

        <?= Html::a("Ejecutar", $this->opciones["url"], $this->opciones["bonito"])?>
    </div>
    
<?php
    }

}
