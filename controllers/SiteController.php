<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Empleado;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    
    public function actionEmpleados() {
        
        // active query clase de yii para consultas sql
        $aq = Empleado::find(); // select * from empleado
        
        // al ejecutar la consulta tengo un array de registros activos
        $vectorAR = $aq->all(); // consulta ejecutada
        
        // leer
        //echo $vectorAR[1]->nombre; // leo el nombre del segundo empleado
        
        // escribir
        // $vectorAR[1]->nombre="jose";
        // $vectorAR[1]->save();
        
        
        // array de arrays
        
        $vectorArrays = $aq->asArray()->all();
        
        // leer 
        // echo $vectorArrays[1]['nombre'];
        
        
        // buscar el empleado cuyo codigo es 1
        
        // $consulta1 = Empleado::findAll([1]);
        
        // $consulta1 = Empleado::findAll(['codigo' => 1]);
        
        // $consulta1 = Empleado::find()->where(['codigo' => 1])->all();
        
        $consulta1 = Empleado::find()->where("codigo=1")->all();
        
        
        
        // quiero mostrar los empleados cuyos codigos estan entre 1 y 5
        
        // $consulta2 = Empleado::find()->where("codigo between 1 and 5")->all();
        // $consulta2 = Empleado::find()->where("codigo>=1 and codigo<=5")->all();  
        // $consulta2 = Empleado::find()->where(['between', 'codigo', 1, 5])->all();
        
        $consulta2 = Empleado::find()
                ->where([
                    "and",
                    ['>=',"codigo", 1],
                    ['<=',"codigo", 5]
                ])->all();
        
        $consulta1 = Empleado::find()->where(['codigo' => 1]); // activeQuery
        
        $consulta2 = Empleado::find()
                ->where([
                    "and",
                    ['>=',"codigo", 1],
                    ['<=',"codigo", 5]
                ]);
        
        // Listar los empleados cuyo departamento es uno
        
        $consulta3 = Empleado::find()->where(['codigo_departamento' => 1]);
        
        
        $dataProvider = new ActiveDataProvider([
            'query' => $consulta2
        ]);
        

        return $this->render('empleados',[
            'registros' => $dataProvider,
        ]);
    }
}
