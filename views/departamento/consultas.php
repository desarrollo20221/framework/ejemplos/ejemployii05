<?php

$this->title = "Listado de empleados de $nombre";
$this->params['breadcrumbs'][] = ['label' => 'Departamentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
use yii\grid\GridView;


echo GridView::widget([
    "dataProvider" => $registros,
]);

