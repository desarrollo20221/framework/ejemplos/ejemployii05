<?php
use yii\helpers\Html;
use yii\widgets\ListView;

echo Html::a("Tabla", ["empleado/$accion", "id"=>1], ["class" => "btn btn-primary"]);

echo ListView::widget([
   "dataProvider" => $registros,
    "itemView" => "_listado",
     "itemOptions" => [
        'class' => 'col-lg-4',
    ],
    "options" => [
        'class' => 'row',
    ],
    'layout'=>"{items}"
]);

