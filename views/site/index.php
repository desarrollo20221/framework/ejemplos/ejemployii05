<?php
use app\widgets\Botones;
use yii\helpers\Html;
use yii\web\View;
/** @var View $this */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Consultas</h1>

        <p class="lead">Ralizando consultas de seleccion</p>
        
    </div>

    <div class="body-content">

        <div class="row">
            <?= Botones::widget([
                "opciones" => [
                    "titulo" => "Consulta 1",
                    "contenido" => "Listar los empleados cuyo departamento es 1",
                    "url" => ["empleado/consulta1","id"=>1],
                    "bonito" => ["class" => "btn btn-primary"]
                ]
            ]) ?>
            
            <?= Botones::widget([
                "opciones" => [
                    "titulo" => "Consulta 2",
                    "contenido" => "Listar los empleado cuyo departamento es el 1 o el 2",
                    "url" => ["empleado/consulta2","id"=>1],
                    "bonito" => ["class" => "btn btn-secondary"]
                ]
            ]) ?>
            
            <?= Botones::widget([
                "opciones" => [
                    "titulo" => "Consulta 3",
                    "contenido" => "Listar los empleados cuyo departamento es desarrollo",
                    "url" => ["empleado/consulta3","id"=>1],
                    "bonito" => ["class" => "btn btn-warning"]
                ]
            ]) ?>
            
            <?= Botones::widget([
                "opciones" => [
                    "titulo" => "Consulta 4",
                    "contenido" => "Listar todos los empleados con el nombre del departamento para el que trabaja",
                    "url" => ["empleado/consulta4"],
                    "bonito" => ["class" => "btn btn-info"]
                ]
            ]) ?>
            
            <?= Botones::widget([
                "opciones" => [
                    "titulo" => "Consulta 5",
                    "contenido" => "Listar el nif, el nombre y los apellidos de los empleados que trabajan en un departamento cuyos gastos exceden los 30000€.
                                    Ademas quiero el nombre de los departamentos y los gastos",
                    "url" => ["empleado/consulta5"],
                    "bonito" => ["class" => "btn btn-danger"]
                ]
            ]) ?>
            
            <?= Botones::widget([
                "opciones" => [
                    "titulo" => "Consulta 6",
                    "contenido" => "Listar todos los empleados (todos los campos) que trabajan en un deparatamento
                                    cuyo presupuesto esta entre 100.000 y 150.000 euros.",
                    "url" => ["empleado/consulta6"],
                    "bonito" => ["class" => "btn btn-dark"]
                ]
            ]) ?>

            <?= Botones::widget([
                "opciones" => [
                    "titulo" => "Consulta 7",
                    "contenido" => "Listar todos los empleados (todos los campos) que trabajan en un deparatamento
                                    cuyo presupuesto esta entre 100.000 y 150.000 euros y cuyo nombre empieze por a.",
                    "url" => ["empleado/consulta7"],
                    "bonito" => ["class" => "btn btn-success"]
                ]
            ]) ?>
                       
        </div>

    </div>
</div>
